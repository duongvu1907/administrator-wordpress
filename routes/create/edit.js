var express = require('express');
var router = express.Router();
var LOG = require('../../repository/log')
var findSite = require('../../repository/find_site')
var findPost = require('../../repository/find_post')
    /* GET users listing. */
router.get('/', function(req, res, next) {
    try {
        if (req.query.type == "site" && req.query.id != "") {
            // console.log(req.query.id)
            // console.log(findSite(parseInt(req.query.id)))
            // res.send("========"+f.id)
            res.render('wordpress/edit_site', { site: findSite(parseInt(req.query.id)) })
        }
        if (req.query.type == "post" && req.query.id != "") {
            res.render('wordpress/edit_post', { post: findPost(parseInt(req.query.id)) })
        }
    } catch (err) {
        console.log(err)
        LOG.error(err.message)
        res.send(err.message)
    }
});

module.exports = router;