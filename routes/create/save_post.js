var express = require('express');
var router = express.Router();
var save = require('../../repository/save')
var dataset = require("../../repository/article")

/* GET users listing. */
router.post('/', function(req, res, next) {
    try {
        var post_current = dataset()
        if (req.body.type == undefined || req.body.type == '') {
            for (let i = 0; i < post_current.length; i++) {
                if (post_current[i].id == req.body.id) {
                    post_current[i].title = req.body.title
                    post_current[i].third = req.body.third
                    post_current[i].second = req.body.second
                    post_current[i].first = req.body.first
                }
            }
        } else {
            var location = -1;
            for (let i = 0; i < post_current.length; i++) {
                if (post_current[i].id == req.body.id) {
                    location = i
                    break
                }
            }
            delete post_current[location]
            post_current = post_current.filter(n => n)
        }
        save(post_current, "/data/sample.json")
        res.send({
            status: '200',
            message: "successful"
        })
    } catch (error) {

        res.send({
            status: "500",
            message: error.message
        })
    }
});

module.exports = router;