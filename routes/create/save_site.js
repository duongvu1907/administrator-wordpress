var express = require('express');
var router = express.Router();
var save = require('../../repository/save')
var dataset = require("../../repository/wordpress")
var findSite = require('../../repository/find_site')
    /* GET users listing. */
router.post('/', function(req, res, next) {
    try {
        var site_current = dataset()
        if (req.body.type == undefined || req.body.type == '') {
            for (let i = 0; i < site_current.length; i++) {
                if (site_current[i].id == req.body.id) {
                    site_current[i].password = req.body.password
                }
            }
        } else {
            var location = -1;
            for (let i = 0; i < site_current.length; i++) {
                if (site_current[i].id == req.body.id) {
                    location = i
                    break
                }
            }
            delete site_current[location]
            site_current = site_current.filter(n => n)
        }
        save(site_current, "/data/wordpress.json")
        res.send({
            status: '200',
            message: "successful"
        })
    } catch (error) {

        res.send({
            status: "500",
            message: error.message
        })
    }
});

module.exports = router;