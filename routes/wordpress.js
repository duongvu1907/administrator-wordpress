var express = require('express');
var router = express.Router();
var dataset = require('../repository/wordpress')
var chrome = require("../repository/puppeteer")
var sortData = require("../repository/sort_data")
var puppeteer = require('puppeteer')
var LOG = require('../repository/log')
    /* GET users listing. */
router.get('/', function(req, res, next) {
    res.render("wordpress/index", { sites: dataset() })
});

router.post('/', function(req, res, next) {
    process(res, req)
});

const process = async(res, req) => {
    var site = req.body.site
    var options = req.body.domain
        // console.log(req.body)
    var domains = await filterOptions(options)
    console.log(domains)
    var name = req.body.name
    var investor = req.body.investor
    var address = req.body.address
    var image = req.body.image
    await posting(domains, {
        site,
        name,
        investor,
        address,
        image
    })
    await res.send("ok")
}

const filterOptions = async(options) => {
    if (!Array.isArray(options)) {
        if (options == "0") {
            return dataset()
        } else {
            return [dataset()[options - 1]]
        }
    } else {
        var data = []
        for (let i = 0; i < options.length; i++) {
            data[i] = dataset()[options[i].id]
        }
        return data
    }
}
const posting = async(domains, data) => {

    // var page = await chrome()

    const browser = await puppeteer.launch({ headless: false, slowMo: 250 });
    const page = await browser.newPage();
    await page.setViewport({ width: 1200, height: 1800 });
    await page.setDefaultNavigationTimeout(0);

    for (let i = 0; i < domains.length; i++) {
        var article = await sortData(data)
        let person = await getInfoStep(domains[i])
        console.log(person.url)
        try {
            console.log("Posting on " + person.domain)
            try {
                await page.goto(person.url)
            } catch (error) {
                await LOG.error(person.url + " Error Posting Cause : " + error.message)
                continue
            }
            var ur = await tabOne(page, person, article, data)
            await LOG.url(ur)
            await LOG.success(person.url + " Wrote Post Success ")
            console.log("Success on " + person.domain)
            console.log("****************FINISH*****************")
        } catch (error) {
            console.log(error)
            await LOG.error(person.url + " Error Posting Cause : " + error.message)
            continue
            // await LOG.error()
        }
    }
    await browser.close()
}
const tabOne = async(page, person, article, data) => {
    var person = person
    var article = article
    var data = data
    console.log("====>LOGIN")
    await page.type('#user_login', person.username)
    await page.type('#user_pass', person.password)
    await page.click("#wp-submit")
    await page.waitForNavigation()
    console.log("====>SUCCESS")
    await page.goto(person.domain + "/wp-admin/post-new.php")
    console.log("========>POSTING")
    await page.click("#content-html")
    await setFeatureImage(page)
    await page.type("#new-tag-post_tag", data.name)
    await page.click(".tagadd")

    await page.evaluate(({ person, article, data }) => {
        document.getElementById("title").value = article.title
        document.getElementById("content").value = article.content

        var categories = document.querySelectorAll("input[name='post_category[]']")
        categories.forEach(cate => {
            cate.checked = true
        })

        if (document.getElementById('focus-keyword-input-metabox')) {
            document.getElementById('focus-keyword-input-metabox').value = data.name
        }
    }, { person, article, data })
    await page.click("#publish")
    await page.waitForSelector("#sample-permalink a");
    console.log("========>PUBLISH")
    var slug = await page.evaluate(() => {
        return document.querySelector("#sample-permalink a").getAttribute("href")
    })
    return slug
}

const getInfoStep = async(person) => {
    return {
        url: person.domain + "/wp-admin",
        username: person.username,
        password: person.password,
        domain: person.domain
    }
}
const setFeatureImage = async(page) => {
    await page.click("#set-post-thumbnail")
        // await page.waitForResponse()
    await page.waitForSelector(".thumbnail");
    await page.click(".thumbnail")

    await page.click(".media-button-select")
}
module.exports = router;