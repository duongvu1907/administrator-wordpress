var express = require('express');
var router = express.Router();
var fs = require("fs")
var LOG = require("../repository/log")
var log_file = "./public/log/wordpress/url.log"
    /* GET users listing. */
router.get('/', function(req, res, next) {
    var logs = fs.readFileSync(log_file, "utf8", (err) => {
        if (err) {
            console.log(err.message)
        }
    })
    logs = logs.split('\n')
    logs = logs.reverse()
        // console.log(logs)
    res.render("log", { logs: logs })
});

module.exports = router;