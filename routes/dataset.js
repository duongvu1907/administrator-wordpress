var express = require('express');
var router = express.Router();
var dataset = require("../repository/wordpress")
var sample = require("../repository/article")
/* GET users listing. */
router.get('/', function(req, res, next) {
  // console.log(sample())
  res.render('dataset/index',{
    sites:dataset(),
    posts:sample()
  })

});

module.exports = router;
