const fs = require('fs')
const dataset = require('./wordpress')
const save = require('./save')
const LOG = require('./log')
module.exports = (data)=>{
    try {
        var sites_available = dataset()
        var id = parseInt(sites_available[sites_available.length-1].id)+1
        sites_available[sites_available.length] = {
            id:id,
            domain:data.domain,
            username:data.username,
            password:data.password
        }
        console.log(sites_available)
        save(sites_available,'/data/wordpress.json')
    } catch (error) {
        console.log(error)
        LOG.error(error.message)
    }

}
