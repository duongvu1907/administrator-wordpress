const fs = require('fs')
const data = "./public/data/wordpress.json"
module.exports = ()=>{
    var raw_data = fs.readFileSync(data,(err)=>{
        if (err) {
            console.log(err)
        }
    })
    return JSON.parse(raw_data)
}