const dataset = require("./article")
var count_data = require("./count_article") 
module.exports = async (data) => {
    // return dataset()
    var data_komdo = await dataset()
    // console.log(data_komdo);
    var count = await count_data()
    var title =  await choose_title(data_komdo,count)
    var content = await choose_content(data_komdo,count)
    return {
        title:replace_data(data,title),
        content:replace_data(data,content)
    }
}
const replace_data = (data,sample)=>{
    var site = data.site
    var name = data.name
    var address  = data.address
    var investor = data.investor
    var image = data.image
    // console.log(sample)
    // console.log(data)
    return sample
            .replace(/{NAME}/g,name)
            .replace(/{ADDRESS}/g,address)
            .replace(/{INVESTOR}/g,investor)
            .replace(/{IMAGE}/g,image)
            .replace(/{LINK}/g,site)

}
const choose_title = async (data_komdo,count)=>{
    var title_id = await parseInt(getRandomInt(0,count))
    // console.log(title_id)
    // console.log(data_komdo[parseInt(title_id)])
    return await data_komdo[title_id].title
}
const choose_content =  async (data_komdo,count)=>{
    var first_box_id =  await parseInt(getRandomInt(0,count))
    var second_box_id = await  parseInt(getRandomInt(0,count))
    var third_box_id = await parseInt(getRandomInt(0,count))

    // console.log(first_box_id+""+data_komdo[first_box_id].first)
    // console.log(second_box_id+""+data_komdo[second_box_id].second)
    // console.log(third_box_id+""+data_komdo[third_box_id].third)

    return data_komdo[first_box_id].first+"<br/>"+data_komdo[second_box_id].second+"<br/>"+data_komdo[third_box_id].third
}
const getRandomInt =   (min, max) => {
    min =  Math.ceil(min);
    max =  Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

