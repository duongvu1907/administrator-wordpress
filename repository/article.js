const fs = require('fs')
const json = "./public/data/sample.json"
module.exports =  ()=>{
    return JSON.parse(fs.readFileSync(json,(err)=>{
        if (err) {
            console.log(err)
        }
    }))
}