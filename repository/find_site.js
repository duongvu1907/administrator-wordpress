const dataset = require("./wordpress")
module.exports =  (id)=>{
    var data = dataset()
    for (let i = 0; i < data.length; i++) {
        if (data[i].id==id) {
            return data[i]
        }
    }
    return null
}