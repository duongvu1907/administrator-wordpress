const fs = require("fs")
const path ="public"
const LOG = require('../repository/log')
module.exports = (data,file) => {
    fs.writeFileSync(path+file, JSON.stringify(data), (err) => {
        console.log(err)
        LOG.error(err.message)
    })
}