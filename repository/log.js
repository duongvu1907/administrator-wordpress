const fs = require('fs')

const error_file = "./public/log/wordpress/error.log"
const success_file = "./public/log/wordpress/success.log"
const url_file = "./public/log/wordpress/url.log"

const success = (message) => {
    var txt = TIMER() + "  !!SUCCESS!!  " + message + "\n"
    fs.appendFile(success_file, txt, (err) => {
        if (err) throw err;
        console.log('The "data to append" was appended to file!');
    });
}
const error = (message) => {
    var txt = TIMER() + " !!ERROR!!  " + message
    fs.appendFile(error_file, txt, (err) => {
        if (err) throw err;
        console.log('The "data to append" was appended to file!');
    });
}
const TIMER = () => {
    var date = new Date()
    var day = date.getDate()
    var month = date.getMonth() + 1
    var year = date.getFullYear()
    var hour = date.getHours()
    var miniute = date.getMinutes()
    var second = date.getSeconds()
    var timezon = "+" + (date.getTimezoneOffset() / 60) * -1
    return `[ ${day}/${month}/${year} ${hour}:${miniute}:${second} GMT ${timezon}] `
}
const url = (url) => {
    var txt = TIMER() + "  !!SUCCESS!!  " + url + "\n"
    fs.appendFile(url_file, txt, (err) => {
        if (err) throw err;
    });
}
module.exports = { success, error, url }