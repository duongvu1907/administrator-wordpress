const puppeteer = require('puppeteer')
module.exports = async ()=>{
    const browser =  await puppeteer.launch({headless:false,slowMo: 250 });
    const page = await browser.newPage();
    await page.setViewport({ width: 1200, height: 1800 });
    await page.setDefaultNavigationTimeout(0);
    return page;
}