var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var wordpress = require("./routes/wordpress");
var baclink = require("./routes/backlink");
var dataset = require("./routes/dataset");
var var_log = require('./routes/test_log');
var app = express();
var add_article = require('./routes/create/article');
var add_site = require('./routes/create/site');
var edit = require('./routes/create/edit');


// ajax route

var ajax_edit_site = require('./routes/create/save_site')
var ajax_edit_post = require('./routes/create/save_post')

// view engine setup
app.set('views', path.join(__dirname, 'views'));
// app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('/wordpress',wordpress);
app.use('/backlink',baclink);
app.use('/dataset',dataset);
app.use('/dataset/add-site',add_site);
app.use('/dataset/add-article',add_article);
app.use('/dataset/edit',edit);
app.use('/test-log',var_log);
// Route Ajax

app.use('/dataset/edit/site',ajax_edit_site)
app.use('/dataset/edit/post',ajax_edit_post)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
