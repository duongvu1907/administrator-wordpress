$(".chosen-select").chosen({ no_results_text: "Oops, nothing found!" });
$("#add-site").click((event) => {

})
$("#add-site").click((event) => {
    $("#create-site").modal('show')
})
$("#add-post").click((event) => {
    $("#create-post").modal('show')
})
$("#change-site").click((event) => {
    var id = parseInt($("#id").val())
    var password = $("#password").val()
    $.post("/dataset/edit/site", {
            id,
            password
        },
        function(data, status) {
            $("#alerts").html("")
            if (data.status == 200) {
                $("#alerts").html(`<div class="alert alert-primary" role="alert">${data.status} - ${data.message}</div>`)
            } else {
                $("#alerts").html(`<div class="alert alert-danger" role="alert">${data.status} - ${data.message}</div>`)
            }
        });
})
$("#change-post").click((event) => {
    var id = parseInt($("#id").val())
    var title = $("#title").val()
    var first = $("#first").val()
    var second = $("#second").val()
    var third = $("#third").val()
    $.post("/dataset/edit/post", {
            id,
            title,
            first,
            second,
            third
        },
        function(data, status) {
            $("#alerts").html("")
            if (data.status == 200) {
                $("#alerts").html(`<div class="alert alert-primary" role="alert">${data.status} - ${data.message} - <a href="/dataset">Back </a></div>`)
            } else {
                $("#alerts").html(`<div class="alert alert-danger" role="alert">${data.status} - ${data.message} - <a href="/dataset">Back</a></div>`)
            }
        });
})
$("#remove-site").click(event => {
    var conf = confirm('Are you sure ? ')
    if (conf) {
        var id = parseInt($("#id").val())
        $.post("/dataset/edit/site", {
                id,
                type: "delete",
            },
            function(data, status) {
                $("#alerts").html("")
                if (data.status == 200) {
                    $("#alerts").html(`<div class="alert alert-primary" role="alert">${data.status} - ${data.message} - <a href="/dataset">Back </a></div>`)
                } else {
                    $("#alerts").html(`<div class="alert alert-danger" role="alert">${data.status} - ${data.message} - <a href="/dataset">Back</a></div>`)
                }
            });
    }
})
$("#remove-post").click(event => {
    var conf = confirm('Are you sure ? ')
    if (conf) {
        var id = parseInt($("#id").val())
        $.post("/dataset/edit/post", {
                id,
                type: "delete",
            },
            function(data, status) {
                $("#alerts").html("")
                if (data.status == 200) {
                    $("#alerts").html(`<div class="alert alert-primary" role="alert">${data.status} - ${data.message} - <a href="/dataset">Back </a></div>`)
                } else {
                    $("#alerts").html(`<div class="alert alert-danger" role="alert">${data.status} - ${data.message} - <a href="/dataset">Back</a></div>`)
                }
            });
    }
})